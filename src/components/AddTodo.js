import React, { Component } from 'react';

class AddTodo extends Component {

  state = {
    content: ''
  }

  handleChange = (event) => {
    this.setState({
      content: event.target.value
    })
  }

  handleSubmit = (event) => {
    event.preventDefault();

    {/* passing the [content] in state via props */}
    this.props.addTodo(this.state)

    // clean input, after add
    this.setState({
      content: ''
    })
  }

  render() {
    return <div>
      <form onSubmit={this.handleSubmit}>
        <label>Add new todo</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.state.content}
        />
        <button type="submit">Add todo</button>
      </form>
    </div>
  }
}

export default AddTodo
